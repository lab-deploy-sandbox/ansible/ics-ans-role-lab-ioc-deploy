# ics-ans-role-lab-ioc-deploy

Ansible role to deploy IOCs in the ICS lab.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-lab-ioc-deploy
```

## License

BSD 2-clause
