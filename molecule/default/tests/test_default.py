import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group')

def test_procserv_installed(host):
    procserv = host.package("procServ")
    assert procserv.is_installed

# FIXME: move test to playbook tests (conserver not installed by this role)
# def test_conserver_running_and_enabled(host):
#     conserver = host.service("conserver")
#     assert conserver.is_running
#     assert conserver.is_enabled

def test_iocuser_and_iocgroup_exist(host):
    iocuser = host.user("iocuser")
    iocgroup = host.group("iocgroup")
    assert iocuser.exists
    assert iocgroup.exists
    assert iocgroup.gid in iocuser.gids

# FIXME: no realtime group in docker container...how to test?
# def test_iocuser_in_realtime_group(host):
#     realtime = host.group("realtime").exists
#     assert realtime.exists
#     assert realtime.gid in host.user("iocuser").gids

def test_e3_prometheus_exporter_running(host):
    exporter = host.service("e3-exporter")
    assert exporter.is_running
    assert exporter.is_enabled
